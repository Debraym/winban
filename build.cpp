#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char** argv) {

	if (argc == 1 || argc > 2){
		cout << "Usage: build *file*.exe" << endl;
		return 0;
	}

	string line;
	char sym1 = '\\', sym2 = '\"', sym3 = '*';

	// Скармливаем скрипту файл из которого будет вытаскивать строки
	// По сути, запаковываем его

	ifstream in(argv[1]);

	ofstream vir;
	vir.open("Ban.cpp"); // На выходе получим исходный код, который нужно будет скомпилировать под винду

	vir << "#include <iostream>" << endl 
		<< "#include <fstream>" << endl 
		<< "using namespace std;" << endl 
		<< "int main() {" << endl 
		<< "ofstream vir;" << endl 
		<< "vir.open(\"" << argv[1] << "\");" << endl;

		// Проходим по строкам файла и приводим в приемлемый для компилятора вид
		if (in.is_open()) {
			cout << "Source code generation..." << endl;
			while(getline(in, line)){

				vir << "vir << \"";

				in >> line;
				for (short i = 0; i < line.length(); i++) {
					
					// TODO: пофиксить проверку, когда первый символ не "вредный"
					if (i == 0 && sym1 == line[i] || i == 0 && sym2 == line[i] || i == 0 && sym3 == line[i]) {
						vir << '\\' << line[i];
					} else if (i != 0 && sym1 == line[i+1] || i != 0 && sym2 == line[i+1] || i != 0 && sym3 == line[i+1]) {
						vir << line[i] << '\\';
					} else if (i == 1 && sym1 == line[i] || i == 1 && sym2 == line[i] || i == 1 && sym3 == line[i]) {
						vir  << '\\' << line[i];
					} else
						vir << line[i];
				
				}

				vir << "\" << endl;" << endl; 
					
			}
		
			cout << "Ban.cpp created successfully" << endl;
		
		} else {
			cout << "Can't open " << argv[1] << endl;
		}
	vir << "return 0;}";

	vir.close();
	in.close();

	return 0;
}
